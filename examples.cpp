// Basic Concepts
// Both of the following programs do the same thing

#include <iostream> // Header
using namespace std; // Programs namespaces

int main(){ // Programs entry point
	string message;
	message = "Test1";
	message = "test1"; // Changes the value of message
	cout << message << endl;
	cout << "Test2";
}

#include <iostream>
using namespace std;

int new_main(){
	string example = "Test1";
	cout << example << endl << "Test2" << endl;
}


// Conditionals and Loops

#include <iostream>
using namespace std;

int main(){
	string name;
	cout << "What is your name" << endl;
	cin >> name;
	if(name == "Johnny"){
		cout << endl << "Hello " + name;	
	}
	else if(name == "Becca"){
		cout << endl << "(>^^)> --^-<@ <(^^<)"; 
	}
	else{
		cout << endl << "Begone Satan!!";
	}
}

// Chaining Conditions

#include <iostream>
using namespace std;

int main(){
	char grade = 'C';
	string name = "Johnny"
	string id = "SU200678779";
	if((name == "Johnny" || id == "SU200678779") && grade == "C"){
		cout << "Yay Assistance!!!"
	}
}

#include <iostream>
using namespace std;

int main(){
	double prices[] = {5.99, 3.2, 9.99, 29.99};
	
	for(int i=0;i<4;i++) { // loops through the array
	  cout << prices[i] << endl;
	}
}

// Using for loop to calculate the items in an array
#include <iostream>
using namespace std;

int main(){
	double prices[] = {5.99, 3.2, 9.99, 29.99};
	
	double total=0;
	for(int i=0;i<4;i++) {
	  total += prices[i];
	}
	cout << total;
}

// Looping with Pointers
#include <iostream>
using namespace std;

int main() {
    
    int ages[] = {19, 24, 36, 45, 56, 52, 21, 27, 24, 34, 29, 60, 40, 42, 45, 47, 22, 30, 34, 20, 18, 26, 51, 43, 47, 39, 22, 34, 56, 52, 21, 27, 24, 37, 19, 24, 36, 45, 44, 49, 23, 25, 19, 40, 29, 60, 40, 42, 45, 47, 61, 30, 19, 43, 47, 39, 41, 46, 29, 24, 21, 25, 28};
    int size = 63;
    int *p = ages;
    int sum = 0;
    int average;

    for(int i=0;i<size;i++) {
        p++; // indexes ages
        sum += *p; // holds value of indexed value
    }
    average = sum / size;
    cout << average;

}

// Using for loops to allocate memory and output the array
#include <iostream>
using namespace std;


int main() {
	int append;
	int ind = 0;
	int size = 8;
    int *p = new int[size];

    for(int i=0;i<size;i++) {
        p[i] = i;
    }

    for(int i=0;i<size;i++) {
       cout << p[i] << endl;
    }
}

// Creating and using classes
#include <iostream>
using namespace std;

class testClass{
	public:
		void youIs(){
			cout << "a bitch" << endl;
		}
};

int main(){
	testClass hey;
	testClass *ptr = &hey; // using pointers to access an objects member
	ptr->youIS();
	hey.youIs();
	return 0;
}

// Explaining to myself
#include <iostream>
using namespace std;

//class definition
class Car{
    //private area
    private:
        int horsepowers;
    //public area
    public:
        void setHorsepowers(int x) {
            horsepowers = x; // users input changing the value of horsepowers in private class
            if(x > 800){
                cout << "Too much" << endl;
            }
        }
        int getHorsepowers() {
            return horsepowers;
        }
};

int main() {
    int horsepowers;
    cin >> horsepowers;
    Car car;
    car.setHorsepowers(horsepowers);
    cout << car.getHorsepowers();

    return 0;
}

// A class with a member intialization list
class MyClass {
 public:
  MyClass(int a, int b)
  : regVar(a), constVar(b)
  {
  }
 private:
  int regVar;
  const int constVar;
};

// Passing arguments through a construct

#include <iostream>
using namespace std;

class Engine {
    public:
        Engine(int p): power(p) {};
        void start() {
            cout <<"Engine ON ("<<power<<" horsepower)";
        }
    private:
        int power;
	friend int turbo(Engine &e, int boost);
};
class Car {
    public:
        Car(Engine x, string c, int y): e(x), color(c), year(y) {};
        void start() {
            cout <<"Starting"<<endl;
            e.start();
            
        }
    private:
        Engine e;
        string color;
        int year;
};

int turbo(Engine &e, int boost){
	return e.power += boost;
}

int main() {
    int power;
    string color;
    int year;
	int boost;
    cin >> power >> color >> year >> boost;
    
    Engine e(power);
    Car car(turbo(e, boost), color, year); // update varibles through the construct
	
    car.start();

}

/* The printInfo() method offers three alternatives for printing the member variable 
 of the class. */

class MyClass {
 public:
  MyClass(int a) : var(a)
  { }
  void printInfo() {
   cout << var<<endl;
   cout << this->var<<endl;
   cout << (*this).var<<endl; 
  }
 private:
  int var;
};