#ifndef EXAMPLE1_H
#define EXAMPLE1_H

class example1{
    public:
        example1(int a, int b);
        ~example1();
        void exampleFunc();
        void exampleFunc() const;
    private:
        int regVar;
        const int constVar;
};

#endif // EXAMPLE1_H