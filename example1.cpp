#include "example1.h"
#include <iostream>
using namespace std;

example1::example1(int a, int b)
: regVar(a), constVar(b)
{
    // creates the constructor declared in the header file
    cout << regVar << endl;
    cout << constVar << endl;
    cout << "Constructor" << endl;
}

example1::~example1(){
    // creates the destructor declared in the header file
    cout << "destructor" << endl;
}
void example1::exampleFunc(){
    cout << "hello" << endl;
}

void example1::exampleFunc() const{
    cout << "I'm a constant" << endl;
}